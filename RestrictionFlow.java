package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class RestrictionFlow {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://spshinde.atlassian.net/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testRestrictionFlow() throws Exception {
    driver.get(baseUrl + "/login?dest-url=/wiki%2Findex.action&permission-violation=true");
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("goodmorningsiddharth@gmail.com");
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("Meghapower101*");
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("quick-create-page-button")).click();
    driver.findElement(By.id("content-title")).clear();
    driver.findElement(By.id("content-title")).sendKeys("This is a page created via selenium automation");
    driver.findElement(By.id("rte-button-publish")).click();
    driver.findElement(By.xpath("//a[@id='action-menu-link']/span/span")).click();
    driver.findElement(By.cssSelector("#action-page-permissions-link > span")).click();
    driver.findElement(By.id("page-restrictions-dialog-save-button")).click();
    driver.findElement(By.id("user-menu-link")).click();
    driver.findElement(By.id("logout-link")).click();
    driver.findElement(By.id("logout")).click();
    driver.findElement(By.cssSelector("button.aui-button.aui-button-link")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
