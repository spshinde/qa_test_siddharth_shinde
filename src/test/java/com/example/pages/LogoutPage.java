package com.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.example.utils.AbstractPage;

public class LogoutPage extends AbstractPage {
	
	@FindBy(id="logout")
	private WebElement logoutButton;
	

	public LogoutPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		loadPage();
	}


	public void logout() {
		// TODO Auto-generated method stub
		logoutButton.click();
	}


	@Override
	public boolean isValidPage() {
		// TODO Auto-generated method stub
		return isElementPresent(By.id("logout"));
	}

}
