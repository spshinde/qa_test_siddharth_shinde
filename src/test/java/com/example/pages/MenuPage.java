package com.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.example.utils.AbstractPage;

public class MenuPage extends AbstractPage {

	@FindBy(xpath = ".//*[@id='content-metadata-page-restrictions']")
	private WebElement pagePermissionButton;

	@FindBy(xpath = ".//*[@id='s2id_page-restrictions-dialog-selector']/a/span[2]")
	private WebElement selectArrow;

	@FindBy(xpath = ".//*[@id='select2-drop']/ul/li[3]/div/div/span[2]")
	private WebElement selectPermission;
	
	@FindBy(id="page-restrictions-dialog-save-button")
	private WebElement saveButton;

	public MenuPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		loadPage();
	}

	public HomePage changePermission() {
		// TODO Auto-generated method stub
		pagePermissionButton.click();
		selectArrow.click();
		selectPermission.click();
		saveButton.click();
		return new HomePage(driver);		
	}

	@Override
	public boolean isValidPage() {
		// TODO Auto-generated method stub
		return isElementPresent(By.xpath(".//*[@id='content-metadata-page-restrictions']"));
	}

}
