package com.example.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.example.utils.AbstractPage;

public class LoginPage extends AbstractPage {

	@FindBy(id = "username")
	private WebElement loginTxt;

	@FindBy(id = "password")
	private WebElement passwordTxt;

	@FindBy(id = "login")
	private WebElement loginButton;

	public LoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		loadPage();
	}

	public LoginPage launchURL(String URL) {
		driver.get(URL);
		return this;
	}

	public LoginPage enterCredentials(String userName, String password) {
		loginTxt.clear();
		loginTxt.sendKeys(userName);

		passwordTxt.clear();
		passwordTxt.sendKeys(password);
		return this;
	}

	public HomePage getHomePage() {
		// TODO Auto-generated method stub
		loginButton.click();
		return new HomePage(driver);
	}

	@Override
	public boolean isValidPage() {
		// TODO Auto-generated method stub
		return false;
	}

}
