package com.example.tests;

import org.junit.*;
import org.openqa.selenium.*;

import com.example.pages.HomePage;
import com.example.pages.LoginPage;
import com.example.pages.LogoutPage;
import com.example.pages.MenuPage;
import com.example.utils.AppUtils;

public class AtlassianTest {
	private WebDriver driver;
	private static String URL = "https://spshinde.atlassian.net";
	private LoginPage loginPage;
	private HomePage homePage;
	private MenuPage menuPage;
	private LogoutPage logoutPage;

	@Before
	public void setUp() throws Exception {
		driver = AppUtils.getDriver();
	}

	@Test
	public void testRestrictionFlow() throws Exception {
		loginPage = new LoginPage(driver);
		homePage = loginPage
				.launchURL(URL)
				.enterCredentials("goodmorningsiddharth@gmail.com",
						"Meghapower101*").getHomePage();
		Assert.assertTrue("Invalid Home Page", homePage.isValidPage());
		menuPage = homePage.createPage().publishPage();
		Assert.assertTrue("Invalid menu Page", menuPage.isValidPage());
		homePage = menuPage.changePermission();
		Assert.assertTrue("Invalid Home Page", homePage.isValidPage());
		logoutPage = homePage.logoutFromDashboard();
		Assert.assertTrue("Invalid Logout Page", logoutPage.isValidPage());
		logoutPage.logout();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
}
