package com.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.example.utils.AbstractPage;

public class HomePage extends AbstractPage {

	@FindBy(id = "quick-create-page-button")
	private WebElement createPageButton;

	@FindBy(id = "content-title")
	private WebElement contentTitle;

	@FindBy(id = "login")
	private WebElement loginButton;

	@FindBy(id = "rte-button-publish")
	private WebElement publishButton;
	
	@FindBy(id="user-menu-link")
	private WebElement menuLink;
	
	@FindBy(id="logout-link")
	private WebElement logoutLink;	
	

	public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		loadPage();
	}

	public HomePage createPage() {
		// TODO Auto-generated method stub
		createPageButton.click();
		contentTitle.clear();
		double randomNumber = (Math.random() * 100) % 1000;
		contentTitle.sendKeys(randomNumber
				+ "This is a page created via selenium automation");
		return this;
	}

	public MenuPage publishPage() {
		// TODO Auto-generated method stub
		publishButton.click();
		return new MenuPage(driver);
	}

	public LogoutPage logoutFromDashboard() {
		// TODO Auto-generated method stub
		menuLink.click();
		logoutLink.click();
		return new LogoutPage(driver);
	}

	@Override
	public boolean isValidPage() {
		// TODO Auto-generated method stub
		return isElementPresent(By.id("quick-create-page-button"));
	}

}
